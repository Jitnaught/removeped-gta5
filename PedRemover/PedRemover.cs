﻿using GTA;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PedRemover
{
    public class PedRemover : Script
    {
        bool deletePed, removeTargetedPed, showSubtitles;
        Keys removalKey;

        string pedCalled;

        public PedRemover()
        {
            deletePed = Settings.GetValue("SETTINGS", "DELETE_PED", true);
            removeTargetedPed = Settings.GetValue("SETTINGS", "TARGETED_PED", true);
            showSubtitles = Settings.GetValue("SETTINGS", "SHOW_SUBTITLES", true);
            removalKey = Settings.GetValue("SETTINGS", "REMOVE_KEY", Keys.X);

            pedCalled = (removeTargetedPed ? "Targeted ped" : "Closest ped");

            KeyDown += PedRemover_KeyDown;
        }

        private Ped GetClosestPedToMe(float radius = 100f)
        {
            Ped me = Game.Player.Character;

            List<Ped> allPeds = new List<Ped>(World.GetAllPeds());

            //only run if greater than 1, because if equals 1 that means the player is the only available ped
            if (allPeds.Count > 1)
            {
                //don't allow player to be returned as closest ped
                allPeds.Remove(me);

                return World.GetClosest(me.Position, allPeds.ToArray());
            }

            return null;
        }

        private void PedRemover_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == removalKey)
            {
                Ped removePed = null;

                if (removeTargetedPed)
                {
                    Entity target = Game.Player.GetTargetedEntity();

                    if (target != null && target.Exists() && target is Ped)
                    {
                        removePed = (Ped)target;
                    }
                }
                else
                {
                    Ped closestPed = GetClosestPedToMe();

                    if (closestPed != null && closestPed.Exists())
                    {
                        removePed = closestPed;
                    }
                }

                if (removePed != null)
                {
                    if (deletePed)
                    {
                        removePed.Delete();
                        if (showSubtitles) UI.ShowSubtitle(pedCalled + " deleted");
                    }
                    else
                    {
                        removePed.MarkAsNoLongerNeeded();
                        if (showSubtitles) UI.ShowSubtitle(pedCalled + " marked no longer needed");
                    }
                }
                else if (showSubtitles) UI.ShowSubtitle("No ped found");
            }
        }
    }
}
